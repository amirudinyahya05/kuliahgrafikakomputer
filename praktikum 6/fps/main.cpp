#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>
#include<stdio.h>

int x=640, y=480, z=-150;
int a1=0, b1=0, sud=0, c1=0;
GLfloat lightposition[]={0,0,0,1};
void pointer(int a,int b ){
a=a-200;b=200-b;
printf("%d %d\n",a,b);

    lightposition[0]=a;
    lightposition[1]=b;
    glLightfv(GL_LIGHT0,GL_POSITION,lightposition);
}
void myKeyboard(unsigned char key, int x, int y) {
	if (key =='w') z+=3;
	if (key =='x') a1+=3;
	if (key =='z') a1-=3;
	else if (key == 's') z-=3;
	else if (key == 'q') {
		a1=0;b1=0;c1=1;
		sud+=15;
	}
	else if (key == 'e') {
		a1=1;b1=0;c1=0;
		sud+=15;
	}
	else if (key == 'd') {
		a1=0;b1=1;c1=0;
		sud+=10;
	}
	else if (key == 'a') {
		a1=0;b1=1;c1=0;
		sud-=10;
	}
}

void init() {

	GLfloat LightPosition[]={0.0f,0.0f,0.0f,1.0f};
	GLfloat LightAmbient[]={0.3f,0.3f,0.3f,1.0f};
	GLfloat LightDiffuse[]={0.7f,0.7f,0.7f,1.0f};
	GLfloat LightSpecular[]={0.5f,0.5f,0.5f,1.0f};
	GLfloat Shine[]={80};

	glClearColor(1,1,1,1);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	glMaterialfv(GL_FRONT,GL_AMBIENT,LightAmbient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,LightDiffuse);
	glLightfv(GL_LIGHT0,GL_POSITION,LightPosition);
	glMaterialfv(GL_FRONT,GL_SHININESS,Shine);
	glMaterialfv(GL_FRONT,GL_SPECULAR,LightSpecular);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30,(GLdouble)x/(GLdouble)y,1, 300);
	glMatrixMode(GL_MODELVIEW);
	glShadeModel(GL_SMOOTH);

//	glFrontFace(GL_CCW);
//	glCullFace(GL_FRONT);
//	glEnable(GL_CULL_FACE);
	glutPassiveMotionFunc(pointer);


}


void resize(int w1, int h1) {
	glViewport(0,0,w1,h1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, (float)w1/(float)h1, 1, 300);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Balok(float p,float l,float x,float y,float z,float sud,float a1,float b1,float c1) {
	glPushMatrix();
	glTranslatef(x, y, z);
	glRotatef(sud, a1, b1, c1);
	glBegin(GL_QUADS);
	glNormal3f(0,0,1);
        glVertex3f( p, l,0);
        glVertex3f(-p, l,0);
        glVertex3f(-p, -l,0);
        glVertex3f( p, -l,0);
    glEnd();
    glPopMatrix();
}
void renderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glClearColor(0,0,0,0); //biru
        glTranslatef(a1, 0, z);
        glRotatef(sud, a1, b1, c1);
        glColor3f(1,0,0);
        //Balok(10,10,0,0,0,10,0,0,0);
        Balok(40,10,10,0,40,-90,0,1,0);
        Balok(10,50,0,10,30,90,1,0,0);
        Balok(40,10,-10,0,40,90,0,1,0);
        Balok(40,10,-50,0,0,-180,1,0,0);
        Balok(40,10,-50,10,-10,90,1,0,0);
        Balok(80,10,-10,0,-20,0,1,0,0);
        Balok(40,10,30,10,-10,90,1,0,0);
         Balok(30,10,40,0,0,180,1,0,0);
        glColor3f(1,1,0);
        Balok(150,150,-50,-10,-10,-90,1,0,0);

        glPushMatrix();
            glRotatef(30,0,1,0);
            glTranslatef(0,-5,0);
            glutSolidCube(5);
        glPopMatrix();
        glPushMatrix();
            glRotatef(80,0,1,0);
            glTranslatef(0,-5,-20);
            glutSolidTorus(2,2,2,5);
        glPopMatrix();
         glPushMatrix();
            glRotatef(80,0,1,0);
            glTranslatef(20,-5,10);
            glutSolidTeapot(5);
        glPopMatrix();
	glutSwapBuffers();
}

void timer(int value) {
	glutPostRedisplay();
	glutTimerFunc(50,timer,0);
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(320,240);
	glutInitWindowSize(x,y);
	glutCreateWindow("yahya amirudin");
	glutDisplayFunc(renderScene);
	glutTimerFunc(50, timer, 0);
	glutKeyboardFunc(myKeyboard);
	glutReshapeFunc(resize);
	init();
	glutMainLoop();
}
