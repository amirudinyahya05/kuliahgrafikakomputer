#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
int a,p,b,c;
void kotak(int posx, int posy,int r,int y){
    glBegin(GL_QUADS);
        glVertex2f(posx-r,posy);
        glVertex2f(posx-r,posy+y);
        glVertex2f(posx+r,posy+y);
        glVertex2f(posx+r,posy);
        glEnd();
}

void segitiga(int posx,int posy, int y,int h){
    glBegin(GL_POLYGON);
        glVertex2f(posx+y,posy);
        glVertex2f(posx,posy+h);
        glVertex2f(posx,posy-h);
    glEnd();
}

void myKeyboard(unsigned char key,int x,int y){
    if(key == 'w'|| key == 'W' ){
        a +=3;
    }else if(key == 'a' || key == 'A'){
        p -=3;
    }else if(key == 's' || key == 'S'){
        a -=3;
    }else if(key == 'd' || key == 'D'){
        p +=3;
    }
}

void mySKeyboard(int key,int x,int y){
    switch(key){
        case GLUT_KEY_LEFT:
            b -=3;
            break;
        case GLUT_KEY_RIGHT:
            b +=3;
            break;
        case GLUT_KEY_UP:
            c +=3;
            break;
        case GLUT_KEY_DOWN:
            c -=3;
            break;
    }
}

void timer(int v){
    glutPostRedisplay();
    glutTimerFunc(50,timer,0);
}

void display(void){
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
    glTranslatef(p,a,0);
    kotak(0,0,5,10);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(b,c,0);
    segitiga(20,5,8,8);
    glPopMatrix();
    glFlush();
}

int main(int argc,char**argv){
    glutInit(&argc,argv);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(400,400);
    glutCreateWindow("kotak segitiga");
    gluOrtho2D(-50,50,-50,50);
    glutDisplayFunc(display);
    glutTimerFunc(1,timer,0);
    glutKeyboardFunc(myKeyboard);
    glutSpecialFunc(mySKeyboard);
    glutMainLoop();
}

