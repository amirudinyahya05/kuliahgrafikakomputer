#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>

int a=640, b=320, c=-10;
int a1=0, b1=0, sud=0, c1=0;


void myKeyboard(unsigned char key, int x, int y) {
	if (key =='w') c+=3;
	else if (key == 's') c-=3;
	else if (key == 'a') {
		a1=0;b1=0;c1=1;
		sud+=15;
	}
	else if (key == 'd') {
		a1=1;b1=0;c1=0;
		sud+=15;
	}
	else if (key == 'x') {
		a1=1;b1=1;c1=0;
		sud+=15;
	}
}

void init() {
	glClearColor(1,1,1,1);
	glEnable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30,(GLdouble)a/(GLdouble)b,1, 300);
	glMatrixMode(GL_MODELVIEW);
	glShadeModel(GL_SMOOTH);
}


void resize(int w1, int h1) {
	glViewport(0,0,w1,h1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, (float)w1/(float)h1, 1, 300);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Balok() {
	glBegin(GL_QUADS);
	glColor3f(1, 0, 0);//sisi atas
        glVertex3f( 2, 1,-1);
        glVertex3f(-2, 1,-1);
        glVertex3f(-2, 1, 1);
        glVertex3f( 2, 1, 1);

    glColor3f(0, 1, 0);//sisi bawah
        glVertex3f( 2,-1,1);
        glVertex3f(-2,-1,1);
        glVertex3f(-2,-1,-1);
        glVertex3f( 2,-1,-1);
    glColor3f(0, 0, 1);//sisi depan
        glVertex3f( 2, 1, 1);
        glVertex3f(-2, 1, 1);
        glVertex3f(-2,-1, 1);
        glVertex3f( 2,-1, 1);
    glColor3f(0, 1, 1);//sisi belakang
        glVertex3f( 2,-1,-1);
        glVertex3f(-2,-1,-1);
        glVertex3f(-2,1,-1);
        glVertex3f( 2,1,-1);
    glColor3f(1,0,1);//sisi kiri
        glVertex3f(-2,1,1);
        glVertex3f(-2,-1,1);
        glVertex3f(-2,-1,-1);
        glVertex3f(-2,1,-1);
    glColor3f(1,1,1);//sisi kanan
        glVertex3f(2,1,-1);
        glVertex3f(2,1,1);
        glVertex3f(2,-1,1);
        glVertex3f(2,-1,-1);
    glEnd();
}

void pintu(){
    glBegin(GL_QUADS);
	 glColor3f(1, 1, 1);//sisi depan
        glVertex3f( -0.5, 0.5, 1.01);
        glVertex3f(-1.5, 0.5, 1.01);
        glVertex3f(-1.5,-1, 1.01);
        glVertex3f( -0.5,-1, 1.01);
    glEnd();
}

void jendela(){
    glBegin(GL_QUADS);
	 glColor3f(1, 1, 1);//sisi depan
        glVertex3f( 1.5, 0.5, 1.01);
        glVertex3f( 1, 0.5, 1.01);
        glVertex3f( 1,-0.5, 1.01);
        glVertex3f( 1.5,-0.5, 1.01);
    glColor3f(1, 1, 1);//sisi depan
        glVertex3f( 0.5, 0.5, 1.01);
        glVertex3f( 0, 0.5, 1.01);
        glVertex3f( 0,-0.5, 1.01);
        glVertex3f( 0.5,-0.5, 1.01);
    glEnd();
}
void atap(){
    glBegin(GL_QUADS);
        glColor3f(1, 1, 1);//bawah
            glVertex3f( 2.2, 1,-1.3);
            glVertex3f(-2.2, 1,-1.3);
            glVertex3f(-2.2, 1, 1.3);
            glVertex3f( 2.2, 1, 1.3);
         glColor3f(01, 0, 0);//depan
            glVertex3f( 2.2, 2, 0);
            glVertex3f(-2.2, 2, 0);
            glVertex3f(-2.2, 1, 1.3);
            glVertex3f( 2.2, 1, 1.3);
        glColor3f(0, 1, 0);//belakang
            glVertex3f( 2.2, 2, 0);
            glVertex3f(-2.2, 2, 0);
            glVertex3f(-2.2, 1, -1.3);
            glVertex3f( 2.2, 1, -1.3);
        glEnd();
}


void renderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glClearColor(0,0,0,0); //biru
	glTranslatef(0, 0, c);
	glRotatef(sud, a1, b1, c1);

	glPushMatrix();
	atap();
	Balok();
	pintu();
	jendela();
	glPopMatrix();
	glutSwapBuffers();
}

void timer(int value) {
	glutPostRedisplay();
	glutTimerFunc(50,timer,0);
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(320,240);
	glutInitWindowSize(a,b);
	glutCreateWindow("yahya amirudin");
	gluOrtho2D(-a/2, a/2, -b/2, b/2);
	glutDisplayFunc(renderScene);
	glutTimerFunc(50, timer, 0);
	glutKeyboardFunc(myKeyboard);
	glutReshapeFunc(resize);
	init();
	glutMainLoop();
}
