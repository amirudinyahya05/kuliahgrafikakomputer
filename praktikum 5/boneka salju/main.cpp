#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <math.h>

int a = 640,b=480,c=-60;
int a1 = 0,b1=0,sud=0,c1=0;

void myKeyboard(unsigned char key,int x,int y){
    if(key == 'w') c+=3;
    else if(key == 's')c-=3;
    else if(key == 'q')a1-=3;
    else if(key == 'e')a1+=3;
    else if(key == 'a'){
        a1=0;b1=0;c1=1;
        sud+=15;
    }
    else if(key == 'd'){
        a1=1;b1=0;c1=0;
        sud+=15;
    }
    else if(key == 'x'){
        a1=0;b1=1;c1=0;
        sud+=15;
    }
}

void init(){
    glViewport(0,0,1,1);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(30,(GLdouble)a/(GLdouble)b,1,300);
    glMatrixMode(GL_MODELVIEW);
    glShadeModel(GL_SMOOTH);
}

void resize(int w1,int h1){
    glViewport(0,0,w1,h1);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,(GLdouble)w1/(GLdouble)h1,1,300);
    glMatrixMode(GL_MODELVIEW);
}

void balok(){
    glBegin(GL_QUADS);
    glColor3f(1,0,0);//sisi atas
        glVertex3f(20,10,-10);
        glVertex3f(-20,10,-10);
        glVertex3f(-20,10,10);
        glVertex3f(20,10,10);
    glColor3f(0,1,0);//sisi bawah
        glVertex3f(20,-10,10);
        glVertex3f(-20,-10,10);
        glVertex3f(-20,-10,-10);
        glVertex3f(20,-10,-10);
    glColor3f(0,0,1);//sisi depan
        glVertex3f(20,10,10);
        glVertex3f(-20,10,10);
        glVertex3f(-20,-10,10);
        glVertex3f(20,-10,10);
    glColor3f(0,1,1);//sisi belakang
        glVertex3f(20,-10,-10);
        glVertex3f(-20,-10,-10);
        glVertex3f(-20,10,-10);
        glVertex3f(20,10,-10);
    glColor3f(1,0,1);//sisi kiri
        glVertex3f(-20,10,10);
        glVertex3f(-20,-10,10);
        glVertex3f(-20,-10,-10);
        glVertex3f(-20,10,-10);
    glColor3f(2,3,0);//sisi kanan
        glVertex3f(20,10,-10);
        glVertex3f(20,10,10);
        glVertex3f(20,-10,10);
        glVertex3f(20,-10,-10);
    glEnd();
}

void pintu(){
    glBegin(GL_QUADS);
    glColor3f(1,1,1);//sisi atas
        glVertex3f(-5,5,10.01);
        glVertex3f(-15,5,10.01);
        glVertex3f(-15,-10,10.01);
        glVertex3f(-5,-10,10.01);
    glEnd();
}

void jendela(){
    glBegin(GL_QUADS);
    glColor3f(1,1,1);
        glVertex3f(15,5,10.01);
        glVertex3f(10,5,10.01);
        glVertex3f(10,-5,10.01);
        glVertex3f(15,-5,10.01);
    glColor3f(1,1,1);
        glVertex3f(5,5,10.01);
        glVertex3f(0,5,10.01);
        glVertex3f(0,-5,10.01);
        glVertex3f(5,-5,10.01);
    glEnd();
}

void atap(){
    glBegin(GL_QUADS);
    glColor3f(1,1,1);
        glVertex3f(22,10,-13);
        glVertex3f(-22,10,-13);
        glVertex3f(-22,10,13);
        glVertex3f(22,10,13);
    glColor3f(1,0,0);
        glVertex3f(22,20,0);
        glVertex3f(-22,20,0);
        glVertex3f(-22,10,13);
        glVertex3f(22,10,13);
    glColor3f(0,1,0);
        glVertex3f(22,20,0);
        glVertex3f(-22,20,0);
        glVertex3f(-22,10,-13);
        glVertex3f(22,10,-13);
    glEnd();
}

void renderScene(void){
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glClearColor(0,0,0,0);
    glTranslatef(a1,0,c);
    glRotatef(sud,a1,b1,c1);

    glPushMatrix();
        atap();
        balok();
        pintu();
        jendela();
    glPopMatrix();
    //bola salju
    glPushMatrix();
        glTranslatef(30,4,0);
        glColor3f(1,1,1);
        glutWireSphere(2,300,50);
    glPopMatrix();

    //tangan kiri
    glPushMatrix();
        glTranslatef(30,3,0);
        glRotatef(90,8,10,0);
        glColor3f(1,0,1);
        GLUquadricObj*quadratic;
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.3,0.3,7,32,32);
    glPopMatrix();
    //jari kiri1
    glPushMatrix();
        glTranslatef(35,-1,0);
        glRotatef(90,14,10,0);
        glColor3f(1,0,0);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.1,0.1,2,32,32);
    glPopMatrix();
    //jari kiri2
    glPushMatrix();
        glTranslatef(35,-1,0);
        glRotatef(90,7,10,0);
        glColor3f(1,0,0);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.1,0.1,2,32,32);
    glPopMatrix();
    //jari kiri3
     glPushMatrix();
        glTranslatef(35,-1,0);
        glRotatef(90,3,10,0);
        glColor3f(1,0,0);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.1,0.1,2,32,32);
    glPopMatrix();

    //tangan kanan
    glPushMatrix();
        glTranslatef(24.5,5,0);
        glRotatef(90,8,10,0);
        glColor3f(1,0,1);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.3,0.3,7,32,32);
    glPopMatrix();
    //jari kanan1
    glPushMatrix();
        glTranslatef(22.5,5,0);
        glRotatef(90,1,10,0);
        glColor3f(1,0,0);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.1,0.1,2,32,32);
    glPopMatrix();
    //jari kanan2
     glPushMatrix();
        glTranslatef(22.5,5.7,0);
        glRotatef(90,3,10,0);
        glColor3f(1,0,0);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.1,0.1,2,32,32);
    glPopMatrix();
    //jari kanan3
     glPushMatrix();
        glTranslatef(23,6.3,0);
        glRotatef(90,7,10,0);
        glColor3f(1,0,0);
        quadratic=gluNewQuadric();
        gluCylinder(quadratic,0.1,0.1,2,32,32);
    glPopMatrix();

    //badan atas
      glPushMatrix();
        glTranslatef(30,0,0);
        glColor3f(1,1,1);
        glutWireSphere(3,300,50);
    glPopMatrix();
    //badan bawah
    glPushMatrix();
        glTranslatef(30,-5,0);
        glColor3f(1,1,1);
        glutWireSphere(4,300,50);
    glPopMatrix();

    //hidung
    glPushMatrix();
        glTranslatef(30,4,0);
        glColor3f(1,0,0);
        glutWireCone(1,3,200,50);
    glPopMatrix();

    //mata kiri
      glPushMatrix();
        glTranslatef(28.9,4.5,2);
        glColor3f(0,0,0);
        glutWireSphere(0.3,200,50);
    glPopMatrix();

     glPushMatrix();
        glTranslatef(30.9,4.5,2);
        glColor3f(0,0,0);
        glutWireSphere(0.2,200,50);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(30,5,0);
        glRotatef(-90,1,0,0);
        glColor3f(0,0,1);
        glutWireCone(2,4,200,50);
    glPopMatrix();
    glutSwapBuffers();
}

void timer(int value){
    glutPostRedisplay();
    glutTimerFunc(50,timer,0);
}

int main(int argc,char**argv){
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE|GLUT_RGBA);
    glutInitWindowPosition(320,240);
    glutInitWindowSize(a,b);
    glutCreateWindow("yahya amirudin");
    gluOrtho2D(-a/2,a/2,-b/2,b/2);
    glutDisplayFunc(renderScene);
    glutTimerFunc(50,timer,0);
    glutKeyboardFunc(myKeyboard);
    glutReshapeFunc(resize);
    init();
    glutMainLoop();
}
